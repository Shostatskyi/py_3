import random
 
country = dict()
 
countries_list = [
  { 'name': 'Thialand', 'country_sea': True, 'country_schengen': False, 'exc_rate': 30.3 },
  { 'name': 'Germany', 'country_sea': True, 'country_schengen': True, 'exc_rate': 60 },
  { 'name': 'Poland', 'country_sea': True, 'country_schengen': True, 'exc_rate': 15 },
  { 'name': 'Hungary', 'country_sea': False, 'country_schengen': True, 'exc_rate': 2 }
  ]
  
countries_dict = {
  'Thialand': { 'country_sea': True, 'country_schengen': False, 'exc_rate': 30.3},
  'Germany':  { 'country_sea': True, 'country_schengen': True, 'exc_rate': 60 },
  'Poland':   { 'country_sea': True, 'country_schengen': True, 'exc_rate': 15 },
  'Hungary':  {'country_sea': False, 'country_schengen': True, 'exc_rate': 2 }
  }
  
countries_dict['Russia'] = {'country_sea': True, 'country_schengen': False, 'exc_rate': 1 }
 
#Используйте форматированный вывод (при помощи % или .format) при вызове print в домашней работе.
for country_name, country_data in countries_dict.items():
  print ('%s has exchange rate %s, and it %s sea' % (country_name, country_data['exc_rate'], 'has' if country_data['country_sea'] else 'does not have'))
 
#Добавьте в программу ещё 5 стран.
countries_dict['France'] = {'country_sea': True, 'country_schengen': True, 'exc_rate': 35.2 }
countries_dict['Italy'] = {'country_sea': True, 'country_schengen': True, 'exc_rate': 45.2 }
countries_dict['China'] = {'country_sea': True, 'country_schengen': False, 'exc_rate': 25 }
countries_dict['Japan'] = {'country_sea': True, 'country_schengen': False, 'exc_rate': 18.1 }
countries_dict['Austria'] = {'country_sea': False, 'country_schengen': True, 'exc_rate': 23.8 }
 
#Добавьте новое свойство «стоимость проживания в сутки в валюте страны».
for country_data in countries_dict.values():
  country_data['cost_day'] = random.randrange(0, 100)
  country_data['ave_tmp'] = random.randrange(0, 30)
  
print('\n', '---------------------')
for country_name, country_data in countries_dict.items():
  print ('\n', country_name, country_data)
 
#Пользуясь множествами, найдите список стран, которые: тёплые и есть море или находятся в шенгене, и нам хватит денег прожить там месяц. 
 
print('\n','---------------------')
 
warm = 20
month_bud = 20000; # rub 
 
schengen = set((c for c, c_d in countries_dict.items() if c_d['country_schengen']))
warm_sea = set((c for c, c_d in countries_dict.items() if c_d['ave_tmp'] > warm and c_d['country_sea']))
afforable = set((c for c, c_d in countries_dict.items() if c_d['cost_day'] * 30 <= month_bud / c_d['exc_rate']))
 
suitable_countries = list((warm_sea & afforable) | (schengen & afforable))
 
print('\n',suitable_countries)
  
#Напишите программу, которая предлагает ввести букву и потом удаляет все вхождения этой буквы в строке "Домашняя работа" - задание убрали, но я решил сделать
 
print('\n','---------------------')
 
st = 'Домашняя работа'
print ('\n', st)
 
while len(st) and st != ' ': 
  inpt = input()
  if isinstance(inpt, str) and len(inpt) == 1:
    st = st.replace(inpt, '')
  print (st)
  